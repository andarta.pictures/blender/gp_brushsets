# GP_Brushsets

save, browse and load brushes/material sets designed per characters

# Installation

Download the repository as a .zip file, access it with blender through Edit/Preferences/Add-ons/Install, then activate it by checking the box in the add-on list.

# User manual not done yet ! look for brushset options in the context menu of the asset browser .


# Warning

This tool is in development stage, thank you for giving it a try ! please report any bug or suggestion to t.viguier@andarta-pictures.com

# Visit us 

[https://www.andarta-pictures.com/](https://www.andarta-pictures.com/)

