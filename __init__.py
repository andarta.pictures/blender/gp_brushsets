# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTIBILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

bl_info = {
    "name" : "GP_Brushsets",
    "author" : "Tom VIGUIER",
    "description" : "Grease Pencil materials and brushes importing system ",
    "blender" : (3, 0, 1),
    "version" : (0, 0, 2),
    "location" : "",
    "warning" : "",
    "category" : "Andarta"
}

import bpy

from bpy.props import (StringProperty,  BoolProperty, EnumProperty)

from bpy.types import AddonPreferences
                     

class GP_PA_AddonPref(AddonPreferences):
    
    bl_idname = __name__

    link_materials : BoolProperty(default = True)
    link : EnumProperty(items = [('LINK', 'Link', 'Link Materials and brushes', 0), ('APPEND', 'Append', 'Append Mateials and brushes', 1)], default = 'APPEND')

    def draw(self, context):
        layout = self.layout
        row = layout.row()
        row.prop(self, 'link', text = "Import mode")

def create_new_asset(name, description):
    new_asset = bpy.data.actions.new(name)
    new_asset.asset_mark()
    new_asset.asset_data.description = description
    return new_asset

def draw_export_context_menu(self, context):
    layout = self.layout
    layout.separator()
    row = layout.row()
    row.label(text = 'GP Brushsets Export')
    row = layout.row()
    row.operator_context = 'INVOKE_DEFAULT'
    row.operator('gp_brushset.mark_asset', text = 'Create Brushset Asset in current file', emboss=True, depress = False)    
               
def draw_import_context_menu(self, context):
    scene = context.scene
    asset = bpy.context.asset_file_handle
    if asset :
        asset_data = asset.asset_data
        if asset_data.description.startswith('GP-BRUSHSET'):
            layout = self.layout
            layout.separator()
            row = layout.row()
            row.label(text = 'GP Brushsets Import')
            row = layout.row()
            ops_1 = row.operator('gp_brushset.import_asset', text = 'Replace with selected Brushset', emboss=True, depress = False) 
            ops_1.replace = True
            row = layout.row()
            ops_2 = row.operator('gp_brushset.import_asset', text = 'Add selected Brushset', emboss=True, depress = False) 
            ops_2.replace = False

class GP_BS_OT_CREATE_ASSET(bpy.types.Operator):     
    bl_label = "Mark Brush set as asset"
    bl_idname = "gp_brushset.mark_asset"

    use_scene_camera : BoolProperty(default = True)
    cancel : BoolProperty(default = False)
    filename_export : StringProperty()
    
    def invoke(self, context, event):
        wm = context.window_manager
        print('invoke')
   
        return wm.invoke_props_dialog(self) 
    
    def draw(self,context):
        layout = self.layout
        layout.use_property_split = True
        split=layout.split()
        col=split.column()
        
        #col.prop(self.gplib_data, 'thumbs_rez', text = 'Thumbnails resolution')
        col.prop(self, 'filename_export', text = 'Brushset name')
        if not context.scene.camera :
            col.label(text = 'You need a camera in the scene to frame thumbnails', icon = 'ERROR')
            #self.cancel = True

    def execute(self, context):
        #print('#------------------------------------------------------#')
        #print('#-------------start mark asset script------------------#')
        #print('#------------------------------------------------------#')
        if not self.cancel :
 
            name = self.filename_export 
            description = 'GP-BRUSHSET'
            linkeds = []
            for brush in bpy.data.brushes :
                if brush.library :
                    linkeds.append('__SUB__:' + brush.library.filepath + '__END__')
            description += ''.join(linkeds)
            new_asset = create_new_asset(name, description)
            new_asset.asset_generate_preview()
            return {'FINISHED'}    
        else:
            return {'CANCELLED'}  

def clean_trash(data_location, list ):
    print('TRASH : ' , list)
    for item in list :
        data_location.remove(item)
    list.clear()
    return list

class GP_BS_OT_IMPORT(bpy.types.Operator) :
    bl_label = "import brush set from .blend"
    bl_idname = 'gp_brushset.importer'
    bl_options = {'REGISTER','UNDO'}

    filepath : StringProperty(
            name="Brushset File Path",
            subtype='FILE_PATH',
            )
    replace : BoolProperty(default = False)

    def execute(self, context):

        print('****************************START IMPORT BRUSHSET*******************************')
        user_preferences = context.preferences
        addon_prefs = user_preferences.addons[__name__].preferences

        used_lib = False
        if addon_prefs.link == 'APPEND':    
            for lib in bpy.data.libraries :
                if self.filepath.endswith(lib.name) :
                    print('altready using :', lib.filepath)
                    used_lib = True        
                    break
        del_brush = []
        del_mat = []       

        if self.replace :        
            #remove existing gp brushes
            for brush in bpy.data.brushes :
                if brush.use_paint_grease_pencil == True :
                    if brush.gpencil_settings and brush.gpencil_settings.use_material_pin and brush.gpencil_settings.material and brush.gpencil_settings.material.users == 1 :                        
                        del_mat.append(brush.gpencil_settings.material)                    
                    del_brush.append(brush)

        del_brush = clean_trash(bpy.data.brushes, del_brush)
        del_mat = clean_trash(bpy.data.materials, del_mat)
       
        if addon_prefs.link == 'LINK' :
            link = True
        if addon_prefs.link == 'APPEND' :
            link = False 
            
        with bpy.data.libraries.load(self.filepath, link = link) as (data_from, data_to):       
            for i, br in enumerate(data_from.brushes):
                if br not in [brush.name for brush in bpy.data.brushes] :
                    data_to.brushes.append(data_from.brushes[i])
                elif [brush for brush in bpy.data.brushes if br == brush.name][0].library and addon_prefs.link == 'APPEND' :
                    brush = [brush for brush in bpy.data.brushes if br == brush.name][0]
                    print('making local :', brush.name)
                    brush.make_local()
                    if brush.gpencil_settings.use_material_pin and brush.gpencil_settings.material and brush.gpencil_settings.material.library :
                        print('making local :', brush.gpencil_settings.material.name )
                        brush.gpencil_settings.material.make_local()
            for brush in data_to.brushes :
                print('BRUSH-----', addon_prefs.link , brush)

        #delete the useless libraries
        if addon_prefs.link == 'APPEND' and not used_lib :    
            for lib in bpy.data.libraries :
                if self.filepath.endswith(lib.name) :
                    print('remove lib :', lib.filepath)
                    bpy.data.libraries.remove(lib)
                    break

        #select a brush
        draw_brushes = [brush for brush in bpy.data.brushes if brush.use_paint_grease_pencil == True and brush.gpencil_tool == 'DRAW']
        if len(draw_brushes) > 0 :
            context.scene.tool_settings.gpencil_paint.brush = draw_brushes[0]  
        
        return {'FINISHED'}

class GP_BRUSHSET_OT_IMPORT_ASSET(bpy.types.Operator):     
    bl_label = "Import brushset asset from Asset Browser"
    bl_idname = "gp_brushset.import_asset"
    #bl_options = {'REGISTER', 'UNDO'}
    replace : BoolProperty(default = False)
    def execute(self, context) :
        
        if bpy.context.area.ui_type == 'ASSETS':
            #print('---')
            asset = bpy.context.asset_file_handle
            asset_data = asset.asset_data
            if asset_data.description.startswith('GP-BRUSHSET'):
                filepath = context.window_manager.asset_path_dummy

                bpy.ops.gp_brushset.importer(filepath = filepath, replace = self.replace)

                #check for sub libraries:
                sublibs = []
                sublib_start = -1
                for i in range(10, len(asset_data.description)):
                    if asset_data.description[i:i+7] == '__SUB__':
                        sublib_start = i+8
                        print('Found a sub lib')
                    if asset_data.description[i:i+7] == '__END__':
                        sublib = asset_data.description[sublib_start:i]
                        print(sublib)
                        sublibs.append(sublib)
                for sublib in sublibs :
                    bpy.ops.gp_brushset.importer(filepath = sublib, replace = False)
        else : 
            print('WRONG AREA CONTEXT')
            pass
        return {'FINISHED'}

#-------REGISTERING ------          
classes = [GP_PA_AddonPref, GP_BS_OT_IMPORT,  GP_BS_OT_CREATE_ASSET , GP_BRUSHSET_OT_IMPORT_ASSET ]               
                        
def register():
    for cls in classes :
        bpy.utils.register_class(cls)
    bpy.types.ASSETBROWSER_MT_context_menu.append(draw_import_context_menu)
    bpy.types.ASSETBROWSER_MT_context_menu.append(draw_export_context_menu)

def unregister():
    for cls in reversed(classes) :
        bpy.utils.unregister_class(cls)
    bpy.types.ASSETBROWSER_MT_context_menu.remove(draw_import_context_menu)
    bpy.types.ASSETBROWSER_MT_context_menu.remove(draw_export_context_menu)
   
if __name__ == "__main__":
    register()
